@extends("layouts.main")

@section("content")
   <div class="places-by-zip-code">
       <h3>Found Places</h3>
       <div class=" places-by-zip-code">

               <table class="table table-striped">
                   <thead>
                   <tr>
                       <th scope="col">Place Name</th>
                       <th scope="col">Longitude</th>
                       <th scope="col">Latitude</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($zipPlaces as $place)
                   <tr>
                       <td>{{ $place->name }}</td>
                       <td>{{ $place->longitude }}</td>
                       <td>{{ $place->latitude }}</td>
                   </tr>
                   @endforeach
                   </tbody>
               </table>
       </div>
   </div>
@endsection