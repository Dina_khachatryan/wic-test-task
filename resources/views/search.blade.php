@extends("layouts.main")

@section("content")

    <div id="form">
        <form class="content" method="post" action="{{ route('findPlacesByZipCode') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <h2>Find Places By Zip Code</h2>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="inputCountry">Country</label>
                    <select class="form-control" id="inputCountry" name="country">
                        <option selected>Choose...</option>
                        <option value="US">United States</option>
                        <option value="CA">Canada</option>
                        <option value="CH">Switzerland</option>
                        <option value="DE">Germany</option>
                        <option value="DK">Denmark</option>
                        <option value="ES">Spain</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GB">Great Britain</option>
                        <option value="GL">Greenland</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="inputZip">Zip</label>
                    <input type="text" class="form-control" id="inputZip" name="code">
                </div>
            </div>
            <div class="errors">
                @foreach ($errors->all() as $message)
                    <span>{{ $message }}</span>
                @endforeach
            </div>
            <button type="submit" name="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
@endsection