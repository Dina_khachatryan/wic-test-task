<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Find zip places</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        html, body {
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }
        h2{
            margin-left: 40px;
            margin-top: 10px;
            font-weight: 600;
        }
        h3{
            text-align: center;
            font-weight: 600;
            margin-top: 30px;
            margin-bottom: 30px;
        }
        #form{
            height: 800px;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .places-by-zip-code{
            width: 80%;
            margin: auto;
        }
        .content {
            height: 400px;
            width: 40%;
            background-color: #dbdddf;
            box-shadow: 4px 4px 5px 5px rgba(34, 60, 80, 0.5);
            border-radius: 8px;
        }
        .content:hover{
            background-color: #e5e7e9;
        }
        .form-row{
            margin-top: 50px;
            margin-left: 40px;
        }
        .form-group{
            padding-bottom: 20px;
        }
        .btn-success{
            margin-left: 40px;
        }
        span{
            margin-left: 40px;
            margin-bottom: 20px;
            font-weight: 600;
            color: #d00000;
        }

    </style>
</head>
<body>
    @yield("content")
</body>
</html>
