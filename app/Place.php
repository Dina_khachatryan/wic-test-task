<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = ['zip_code_id', 'name', 'longitude', 'latitude'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zipCode(){
        return $this->belongsTo(ZipCode::class, 'zip_code_id', 'id');
    }
}
