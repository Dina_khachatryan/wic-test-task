<?php

namespace App\Http\Controllers;

use App\Place;
use App\ZipCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ZipCodeController extends Controller
{
    public function searchPage()
    {
        return view('search');
    }

    public function findPlacesByZipCode(Request $request)
    {
        $this->validate($request, [
            'country' => 'required',
            'code' => 'required'
        ]);

        $countryValue = $request->get('country');
        $zipCodeValue = $request->get('code');

        $zipPlaces = [];

        $zipCode = ZipCode::where('zip_code', $zipCodeValue)->first();

        if ($zipCode) {
            $zipPlaces = $zipCode->places;
        } else {
            try {
                $getPlacesByZipRequest = Http::get("http://api.zippopotam.us/$countryValue/$zipCodeValue");
            }
            catch(\Exception $e) {
                return redirect()->back()->withErrors(['There was an error while trying to find the zip places, please try again in 5 minutes.']);
            }

            $getPlacesByZipResponse = $getPlacesByZipRequest->json();

            if(empty($getPlacesByZipResponse)){
                return redirect()->back()->withErrors(['Zip code does not exist for the selected country.']);
            }

            $zipCodeModel = ZipCode::create([
                'zip_code' => $getPlacesByZipResponse['post code'],
                'country' => $getPlacesByZipResponse['country']
            ]);

            foreach ($getPlacesByZipResponse['places'] as $place) {
                if (!isset($place['place name']) || !isset($place['longitude']) || !isset($place['latitude'])) continue;

                $place = Place::create([
                    'zip_code_id' => $zipCodeModel->id,
                    'name' => $place['place name'],
                    'longitude' => $place['longitude'],
                    'latitude' => $place['latitude']
                ]);

                $zipPlaces[] = $place;
            }
        }

        return view('places', compact('zipPlaces'));
    }
}
